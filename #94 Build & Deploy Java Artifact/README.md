# Build & Deploy Java Artifact Project

Given the requirements for building and deploying a Java application on a remote Ubuntu server using Ansible, I'll guide you through creating an Ansible playbook that compiles a Java-Gradle project, manages a Linux user on the remote server, and deploys the built JAR artifact. This playbook assumes you have already set up your Ubuntu server and configured SSH access according to the preparation steps provided.

## Preparation
Before proceeding with the Ansible playbook, ensure:

- You've installed Ansible on your local machine.
- The remote Ubuntu server is accessible via SSH using the provided key.
- Java and Gradle are installed on your local machine to build the project.
- The acl package is installed on the remote server as described.

## Ansible Playbook: 1-build-and-deploy.yaml
This playbook consists of multiple tasks: building the Java application, ensuring the specified Linux user exists on the remote server, stopping any currently running instance of the application, and deploying the new JAR file.

```yaml
---
- name: Build and Deploy Java Application
  hosts: all
  vars_prompt:
    - name: linux_user
      prompt: "Enter the Linux username"
      private: no

  vars:
    project_dir: "/path/to/your/java/project" # Update this path
    jar_name: "application.jar" # Update with your JAR file name

  tasks:
    - name: Build the Java project with Gradle
      local_action:
        module: command
        cmd: gradle build
      args:
        chdir: "{{ project_dir }}"

    - name: Check if the Linux user exists
      user:
        name: "{{ linux_user }}"
        state: present
        shell: /bin/bash
        createhome: yes
      become: yes

    - name: Stop the currently running Java application (if any)
      shell: pkill -f "{{ jar_name }}"
      ignore_errors: yes
      become: yes
      become_user: "{{ linux_user }}"

    - name: Remove the old JAR file (if exists)
      file:
        path: "/home/{{ linux_user }}/{{ jar_name }}"
        state: absent
      become: yes
      become_user: "{{ linux_user }}"

    - name: Copy the new JAR file to the remote server
      copy:
        src: "{{ project_dir }}/build/libs/{{ jar_name }}"
        dest: "/home/{{ linux_user }}/{{ jar_name }}"
        mode: '0755'
      become: yes
      become_user: "{{ linux_user }}"

    - name: Start the Java application
      shell: nohup java -jar "/home/{{ linux_user }}/{{ jar_name }}" > app.log 2>&1 &
      become: yes
      become_user: "{{ linux_user }}"
      async: 10
      poll: 0
```

## Instructions:
1. Replace /path/to/your/java/project with the absolute path to your Java-Gradle project on your local machine.
2. Update application.jar with the actual name of your JAR file as built by Gradle.
3. Save this playbook as 1-build-and-deploy.yaml.
4. Ensure you have an inventory file hosts-deploy-app configured with your remote server details.
5. Run the playbook using the command provided, replacing "your-name" with your actual Linux username and /absolute/path/to/java/gradle/project with the correct path.

Command to Run the Playbook:

```bash
ansible-playbook -i hosts-deploy-app 1-build-and-deploy.yaml --extra-vars "project_dir=/absolute/path/to/java/gradle/project jar_name=application.jar"
```

This playbook automates the process of building, stopping any currently running instance, and deploying a Java application, streamlining the deployment process for developers.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
