# Push Java Artifact to Nexus Project

To facilitate the process of uploading a built Java artifact (JAR file) to a Nexus repository after testing, an Ansible playbook can be created to automate this task. This approach streamlines the deployment process, allowing developers to push successful artifacts to a shared Nexus repository easily. Below is an example playbook 2-push-to-nexus.yaml designed to upload a specified JAR file to a Nexus repository.

## Assumptions:
A Nexus Repository Manager is already set up and accessible.
The Nexus instance has a repository configured for Maven snapshots (maven-snapshots or similar).
Credentials (username and password) for Nexus are available and have the necessary permissions to upload artifacts.

## Playbook: 2-push-to-nexus.yaml

```yaml
---
- name: Push JAR artifact to Nexus Repository
  hosts: localhost
  gather_facts: no
  vars_prompt:
    - name: nexus_url
      prompt: "Enter Nexus URL (e.g., http://nexus-ip:nexus-port)"
      private: no
    - name: nexus_user
      prompt: "Enter Nexus username"
      private: no
    - name: nexus_password
      prompt: "Enter Nexus password"
      private: yes
    - name: repository_name
      prompt: "Enter Nexus repository name (e.g., maven-snapshots)"
      private: no
    - name: artifact_name
      prompt: "Enter artifact name"
      private: no
    - name: artifact_version
      prompt: "Enter artifact version (e.g., 1.0-SNAPSHOT)"
      private: no
    - name: jar_file_path
      prompt: "Enter absolute path to JAR file"
      private: no

  tasks:
    - name: Upload JAR file to Nexus
      uri:
        url: "{{ nexus_url }}/repository/{{ repository_name }}/{{ artifact_name }}/{{ artifact_version }}/{{ artifact_name }}-{{ artifact_version }}.jar"
        method: PUT
        user: "{{ nexus_user }}"
        password: "{{ nexus_password }}"
        force_basic_auth: yes
        body_format: raw
        src: "{{ jar_file_path }}"
        status_code: 201,204
        timeout: 60
      delegate_to: localhost
```

## Instructions to Run the Playbook:
1. Save the playbook as 2-push-to-nexus.yaml.
2. Execute the playbook using the command provided. Replace placeholder values (e.g., nexus-ip:nexus-port, admin-pass, etc.) with actual values relevant to your Nexus setup and the artifact you're uploading.

```bash
ansible-playbook 2-push-to-nexus.yaml
```

## Note:
- The playbook uses the uri module to make a PUT request to Nexus, uploading the specified JAR file.
- vars_prompt is used to gather necessary information from the user running the playbook, including Nexus credentials, repository details, and artifact information.
- Since the operation is performed locally to push a file to a remote repository, hosts: localhost and delegate_to: localhost are used, eliminating the need for an inventory file.
- Ensure the Nexus URL, repository name, and artifact details are correctly specified to successfully upload the artifact.
This playbook empowers developers to automate the upload of Java artifacts to a Nexus repository, facilitating a smooth transition from local development and testing to sharing artifacts within a team or organization.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
