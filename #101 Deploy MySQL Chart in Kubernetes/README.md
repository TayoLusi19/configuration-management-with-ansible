To address the team's concern about application availability and to run the MySQL database in multiple replicas, deploying a MySQL chart with 3 replicas using a Helm chart via an Ansible script is an effective solution. This approach enhances database availability and resilience within your Kubernetes cluster. Here's how you can achieve this:

Step 1: Preparation
Ensure you have Helm installed on your Ansible control machine or wherever you plan to run the Ansible playbook from. Also, ensure that the Kubernetes cluster is accessible and that the KUBECONFIG environment variable is correctly set.

Step 2: Create Ansible Playbook
Create an Ansible playbook named 8-deploy-on-k8s.yaml. This playbook will use Helm to deploy a MySQL chart configured for 3 replicas.

```yaml
---
- name: Deploy MySQL with Helm in Kubernetes
  hosts: localhost
  environment:
    KUBECONFIG: "/path/to/kube/config/file"
  tasks:
    - name: Remove existing MySQL deployment
      community.kubernetes.k8s:
        api_version: apps/v1
        kind: Deployment
        name: mysql-deployment
        namespace: default
        state: absent

    - name: Add Bitnami Helm repository
      community.general.helm_repository:
        name: bitnami
        repo_url: https://charts.bitnami.com/bitnami

    - name: Update Helm repository
      community.general.helm:
        command: repo update

    - name: Deploy MySQL with 3 replicas
      community.general.helm:
        name: my-mysql
        chart_ref: bitnami/mysql
        release_namespace: default
        create_namespace: yes
        values: 
          architecture: replication
          replicaCount: 3
          auth:
            rootPassword: "securepassword"
            database: "myapp_db"
            username: "dbuser"
            password: "dbpassword"
```

This playbook performs the following actions:

Removes the existing MySQL deployment, if present.
Adds the Bitnami Helm repository and updates it.
Deploys the MySQL chart from the Bitnami repository with the desired configuration for replication and 3 replicas.
Step 3: Execute the Playbook
Run the playbook using the following command:

```bash
ansible-playbook 8-deploy-on-k8s.yaml --extra-vars "docker_user=your-dockerhub-user docker_pass=your-dockerhub-password"
```

Ensure to replace "your-dockerhub-user" and "your-dockerhub-password" with your DockerHub credentials if needed. This command assumes that Docker credentials are required for pulling any private images used in the Kubernetes cluster.

Verification
After the playbook execution, verify that the MySQL deployment with 3 replicas is successfully running:

```bash
kubectl get pods -l app=my-mysql -n default
```

You should see 3 pods for MySQL indicating that the deployment is successful and the replicas are up and running.

# Conclusion
By automating the deployment of a MySQL chart with high availability in Kubernetes using Ansible, you've significantly reduced manual work and enhanced the DevOps processes within your project. This setup not only ensures greater availability for the MySQL database but also demonstrates a commitment to modernizing infrastructure and embracing automation.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
