To support creating Jenkins instances on both Amazon Linux and Ubuntu servers dynamically, you can refactor your Ansible playbooks to use conditional execution based on the operating system specified. This approach allows for a more flexible and maintainable setup. Here's how you can organize your playbooks and use include_tasks or conditionals to achieve this:

Step 1: Provisioning the EC2 Server
Keep the 3-provision-jenkins-ec2.yaml playbook for provisioning the EC2 instance. This playbook remains largely the same for both Amazon Linux and Ubuntu, with the key difference being the ami_id used.

3-provision-jenkins-ec2.yaml
This playbook is used to provision the EC2 server and doesn't need to be changed from your original setup. Just ensure you provide the correct ami_id for Amazon Linux or Ubuntu as needed.

Step 2: Installing Jenkins on the EC2 Server
Create a playbook named 4-install-jenkins-ubuntu.yaml that will include tasks or conditionals to install Jenkins and other software based on the operating system.

4-install-jenkins-ubuntu.yaml

```yaml
---
- name: Install Jenkins on EC2
  hosts: all
  become: yes
  vars:
    host_os: ubuntu  # Default to ubuntu, can be overridden by --extra-vars

  tasks:
    - name: Include tasks for Amazon Linux
      include_tasks: 4-host-amazon.yaml
      when: host_os == "amazon-linux"

    - name: Include tasks for Ubuntu
      include_tasks: 4-host-ubuntu.yaml
      when: host_os == "ubuntu"
```

OS-specific Task Files
Create two separate task files 4-host-amazon.yaml and 4-host-ubuntu.yaml for Amazon Linux and Ubuntu, respectively. These files will contain the specific commands and packages for each operating system.

4-host-amazon.yaml

```yaml
---
- name: Install Java for Amazon Linux
  yum:
    name: java-1.8.0-openjdk
    state: present

# Add more tasks specific to Amazon Linux
```

4-host-ubuntu.yaml

```yaml
---
- name: Install Java for Ubuntu
  apt:
    name: openjdk-8-jdk
    state: present
    update_cache: yes

# Add more tasks specific to Ubuntu
```

Executing the Playbooks
For an Ubuntu EC2 instance:

```bash
ansible-playbook 3-provision-jenkins-ec2.yaml --extra-vars "ssh_key_path=/path/to/ssh-key aws_region=your-aws-region key_name=your-key-pair-name subnet_id=your-subnet-id ami_id=ubuntu-ami-id ssh_user=ubuntu"

ansible-playbook -i hosts-jenkins-server 4-install-jenkins-ubuntu.yaml --extra-vars "host_os=ubuntu aws_region=your-aws-region"
```

For an Amazon Linux EC2 instance:

```bash
ansible-playbook 3-provision-jenkins-ec2.yaml --extra-vars "ssh_key_path=/path/to/ssh-key aws_region=your-aws-region key_name=your-key-pair-name subnet_id=your-subnet-id ami_id=amazon-linux-ami-id ssh_user=ec2-user"

ansible-playbook -i hosts-jenkins-server 4-install-jenkins-ubuntu.yaml --extra-vars "host_os=amazon-linux aws_region=your-aws-region"
```

This approach allows you to dynamically select the set of tasks to execute based on the host_os variable, making your playbook versatile for different operating systems while keeping your configuration DRY (Don't Repeat Yourself).

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
