
To facilitate the deployment of a Java-MySQL application to Kubernetes for a team transitioning to modern infrastructure, you can automate the process using Ansible. This involves creating Docker images, Kubernetes manifests for deployment, services, ConfigMaps, Secrets, and setting up an Nginx Ingress controller to access the web application from a browser. Here's a detailed approach:

Dockerization and Image Preparation
Dockerize the Java-MySQL Application: Refer to the provided Dockerfile in the solutions branch to containerize your application. The Dockerfile should define the base image, build instructions, and the CMD or ENTRYPOINT instructions to run the application.

Build and Push Docker Image:

Navigate to the directory containing your Dockerfile.
Build the Docker image: docker build -t your-docker-hub-id/demo-app:java-mysql-app .
Log in to DockerHub: docker login
Push the built image: docker push your-docker-hub-id/demo-app:java-mysql-app
Kubernetes Cluster Setup
Create a Kubernetes cluster using your preferred method (Minikube, LKE, EKS, etc.), and obtain the kubeconfig file to interact with the cluster.

Kubernetes Manifests Preparation
Prepare Kubernetes manifests for deployments, services, ConfigMaps, Secrets, and Ingress for both the Java and MySQL applications. Place these manifests in a directory named kubernetes-manifests/exercise-7/.

Ansible Automation
Create an Ansible playbook 7-deploy-on-k8s.yaml to automate the deployment of these Kubernetes resources.

```yaml
---
- name: Deploy Java MySQL Application on Kubernetes
  hosts: localhost
  tasks:
    - name: Set KUBECONFIG environment variable
      ansible.builtin.set_fact:
        kubeconfig: "/path/to/kube/config/file"

    - name: Log in to DockerHub
      community.general.docker_login:
        username: "{{ docker_user }}"
        password: "{{ docker_pass }}"
        email: your-email@example.com

    - name: Deploy Kubernetes manifests
      community.kubernetes.k8s:
        kubeconfig: "{{ kubeconfig }}"
        state: present
        src: "{{ item }}"
      loop:
        - "kubernetes-manifests/exercise-7/mysql-deployment.yaml"
        - "kubernetes-manifests/exercise-7/java-app-deployment.yaml"
        - "kubernetes-manifests/exercise-7/java-app-service.yaml"
        - "kubernetes-manifests/exercise-7/java-app-configmap.yaml"
        - "kubernetes-manifests/exercise-7/java-app-secret.yaml"
        - "kubernetes-manifests/exercise-7/java-app-ingress.yaml"
```

Ensure you have the community.kubernetes collection installed (ansible-galaxy collection install community.kubernetes) to use the k8s module in Ansible.

Execution
Run the Ansible playbook to deploy your Kubernetes resources:

```bash
ansible-playbook 7-deploy-on-k8s.yaml --extra-vars "docker_user=your-dockerhub-user docker_pass=your-dockerhub-password"
```

Replace your-dockerhub-user and your-dockerhub-password with your DockerHub credentials. This playbook will deploy all necessary Kubernetes resources to run your Java-MySQL application.

Accessing the Application
After deployment, access the Java application via the browser using the host set in java-app-ingress.yaml. Ensure the Ingress controller is properly set up and that the DNS or /etc/hosts file is configured to resolve the host to the Ingress controller's IP.

This comprehensive Ansible automation simplifies the deployment process, allowing the team to deploy their Java-MySQL application to Kubernetes without deep knowledge of Kubernetes commands or configuration syntax.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
