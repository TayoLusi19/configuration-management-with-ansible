
To meet the requirements of running Jenkins as a Docker container, you can create an Ansible playbook that sets up Docker and runs Jenkins within a container on your server. This setup will allow your team to leverage Docker for Jenkins deployments, providing flexibility and simplicity in managing Jenkins instances.

Step 1: Provision the EC2 Server
Use the 3-provision-jenkins-ec2.yaml playbook to provision an EC2 instance where Docker and Jenkins will be installed. This step remains the same as in the previous exercises.

Step 2: Install Jenkins as a Docker Container
Create a new playbook named 5-install-jenkins-docker.yaml for installing Docker on the server and running Jenkins as a Docker container. This playbook will ensure Docker is installed and then execute the Docker command to start Jenkins, mapping the necessary volumes and ports.

5-install-jenkins-docker.yaml

```bash
---
- name: Install Jenkins in Docker
  hosts: all
  become: yes
  tasks:
    - name: Install Docker
      apt:
        name: docker.io
        state: latest
        update_cache: yes

    - name: Add current user to Docker group
      user:
        name: "{{ ansible_user }}"
        group: docker
        append: yes

    - name: Ensure Docker service is running
      service:
        name: docker
        state: started
        enabled: yes

    - name: Pull Jenkins Docker image
      docker_image:
        name: jenkins/jenkins:lts
        source: pull

    - name: Create Jenkins Docker container
      docker_container:
        name: jenkins
        image: jenkins/jenkins:lts
        state: started
        restart_policy: always
        published_ports:
          - "8080:8080"
          - "50000:50000"
        volumes:
          - "/var/run/docker.sock:/var/run/docker.sock"
          - "/usr/local/bin/docker:/usr/bin/docker"
          - "jenkins_home:/var/jenkins_home"
```

Execution Instructions:
Provision the Ubuntu EC2 instance using the 3-provision-jenkins-ec2.yaml playbook as described in previous exercises.
Run the 5-install-jenkins-docker.yaml playbook to install Docker and Jenkins on the server:

```bash
ansible-playbook -i hosts-jenkins-server 5-install-jenkins-docker.yaml --extra-vars "aws_region=your-aws-region"
```

Make sure to replace "aws_region=your-aws-region" with the actual AWS region and adjust any other necessary variables as per your setup. This command assumes you have an inventory file named hosts-jenkins-server that lists the provisioned server(s).

Note:
This playbook uses the docker_image and docker_container modules from the community.docker collection. If you haven't used these modules before, you may need to install the collection using the following command:

```bash
ansible-galaxy collection install community.docker
```

This setup gives your team the flexibility to quickly spin up a Jenkins server running inside a Docker container, simplifying the management and deployment of Jenkins instances for various needs.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
