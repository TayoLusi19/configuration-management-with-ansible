# Install Jenkins on EC2 Project

To fulfill the requirements of creating a new EC2 server and setting up Jenkins along with Node.js, NPM, and Docker for Jenkins builds, you will need two main Ansible playbooks. The first playbook will provision the EC2 instance, and the second will configure Jenkins and the required tools on the provisioned server.

## Assumptions:
You have Ansible installed on your local machine.
You have an AWS account and have set up AWS CLI with necessary permissions.
You've created an SSH key pair in AWS for EC2 access.

## 1. Provisioning EC2 Server: 3-provision-jenkins-ec2.yaml
```yaml
---
- name: Provision Jenkins EC2 Server
  hosts: localhost
  gather_facts: no
  vars:
    instance_type: t2.micro
    security_group_id: sg-12345 # Update with your SG ID
    image_id: ami-123456 # Update with Amazon Linux 2 AMI ID for your region
    key_name: my-key-pair # Update with your key pair name
    subnet_id: subnet-6789abcd # Update with your subnet ID
    region: us-east-1 # Update with your AWS region
    tag_name: JenkinsServer

  tasks:
    - name: Launch EC2 Instance
      ec2:
        key_name: "{{ key_name }}"
        instance_type: "{{ instance_type }}"
        image: "{{ image_id }}"
        wait: true
        group_id: "{{ security_group_id }}"
        vpc_subnet_id: "{{ subnet_id }}"
        region: "{{ region }}"
        assign_public_ip: true
        instance_tags:
          Name: "{{ tag_name }}"
      register: ec2

    - name: Add new instance to host group
      add_host:
        hostname: "{{ item.public_ip }}"
        groupname: launched
      loop: "{{ ec2.instances }}"
```

## 2. Installing Jenkins on EC2: 3-install-jenkins-ec2.yaml

```yaml
---
- name: Install and Configure Jenkins on EC2
  hosts: launched
  become: yes
  vars:
    jenkins_home: /var/lib/jenkins

  tasks:
    - name: Update and install Java, Node.js, NPM, and Docker
      yum:
        name:
          - java-1.8.0-openjdk
          - nodejs
          - npm
          - docker
        state: present

    - name: Start and enable Docker service
      service:
        name: docker
        state: started
        enabled: true

    - name: Add ec2-user to the Docker group
      user:
        name: ec2-user
        groups: docker
        append: yes

    - name: Download and install Jenkins
      get_url:
        url: "https://pkg.jenkins.io/redhat-stable/jenkins.war"
        dest: "/opt/jenkins.war"
        mode: '0755'

    - name: Start Jenkins
      shell: nohup java -jar /opt/jenkins.war --httpPort=8080 > /dev/null 2>&1 &
```

## Execution Instructions:
Provision the EC2 Server: Run the first playbook to provision the EC2 instance. Replace the placeholder values (/path/to/ssh-key/file, your-aws-region, etc.) with your actual values before execution.

```bash
ansible-playbook 3-provision-jenkins-ec2.yaml --extra-vars "ssh_key_path=/path/to/ssh-key aws_region=us-east-1 key_name=my-key-pair subnet_id=subnet-123456 ami_id=ami-123456 ssh_user=ec2-user"
```

Configure Jenkins on the Server: After the server is provisioned and accessible, run the second playbook to install and configure Jenkins and other required software. You might need to create a dynamic inventory or update your inventory file (hosts-jenkins-server) with the new EC2 instance's IP.

```bash
ansible-playbook -i hosts-jenkins-server 3-install-jenkins-ec2.yaml --extra-vars "aws_region=us-east-1"
```

These playbooks will create a new EC2 instance, install Jenkins, and prepare the server for Jenkins builds with Node.js, NPM, and Docker installed. Remember to replace placeholders with your actual values related to AWS and ensure the security group allows inbound traffic on port 8080 for Jenkins access.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
