To automate deploying and configuring web and database servers on AWS, including a dedicated Ansible server for orchestration, follow this structured approach with Ansible. This setup involves provisioning EC2 instances for the Ansible control plane, web server, and database server, with configurations for security and access, and installations for Jenkins, MySQL, and the Java application. Below are the steps and playbook examples to achieve this.

Step 1: Provision the Ansible Control Server
Create 6-provision-ansible-server.yaml to provision a dedicated Ansible control server on AWS EC2.

```yaml
---
- name: Provision Ansible Control Server
  hosts: localhost
  gather_facts: no
  tasks:
    - name: Launch Ansible Control EC2 Instance
      amazon.aws.ec2_instance:
        name: "Ansible-Control-Server"
        key_name: "{{ key_name }}"
        instance_type: "t2.micro"
        image_id: "{{ ami_id }}"
        vpc_subnet_id: "{{ subnet_id }}"
        region: "{{ aws_region }}"
        assign_public_ip: true
        security_group: "Ansible-SG"
      vars:
        aws_region: "eu-west-3"
        key_name: "ansible-control-server-key"
        subnet_id: "public-subnet-id"
        ami_id: "ami-0c6ebbd55ab05f070"
```

Step 2: Configure the Ansible Control Server
Create 6-configure-ansible-server.yaml to configure the Ansible server with the necessary tools and playbooks.

```yaml
---
- name: Configure Ansible Control Server
  hosts: ansible_control
  become: yes
  tasks:
    - name: Install Ansible and Git
      apt:
        name:
          - ansible
          - git
        state: latest
        update_cache: yes

    - name: Clone Ansible Playbooks Repository
      git:
        repo: "https://your-repo-url.git"
        dest: "/home/ubuntu/ansible-playbooks"
```

Step 3: Provision Web and Database Servers
Create 6-provision-app-servers.yaml to provision both the web server and database server.

```yaml
---
- name: Provision Application Servers
  hosts: localhost
  gather_facts: no
  tasks:
    - name: Launch Web Server EC2 Instance
      amazon.aws.ec2_instance:
        name: "Web-Server"
        key_name: "{{ key_name }}"
        instance_type: "t2.micro"
        image_id: "{{ ami_id }}"
        vpc_subnet_id: "{{ subnet_id_web }}"
        region: "{{ aws_region }}"
        assign_public_ip: true
        security_group: "Web-SG"
      vars:
        aws_region: "eu-west-3"
        key_name: "ansible-managed-server-key"
        subnet_id_web: "public-subnet-id"
        ami_id: "ami-0c6ebbd55ab05f070"

    - name: Launch Database Server EC2 Instance without Public IP
      amazon.aws.ec2_instance:
        name: "Database-Server"
        key_name: "{{ key_name }}"
        instance_type: "t2.micro"
        image_id: "{{ ami_id }}"
        vpc_subnet_id: "{{ subnet_id_db }}"
        region: "{{ aws_region }}"
        assign_public_ip: false
        security_group: "DB-SG"
      vars:
        subnet_id_db: "private-subnet-id"
```

Step 4: Configure Web and Database Servers
Create 6-configure-app-servers.yaml to configure both servers, installing MySQL on the database server and deploying the Java application on the web server.

```yaml
---
- name: Configure Database Server
  hosts: db_server
  become: yes
  roles:
    - role: geerlingguy.mysql

- name: Configure Web Server
  hosts: web_server
  become: yes
  tasks:
    - name: Install Java
      apt:
        name: openjdk-11-jdk
        state: present

    - name: Deploy Java Application
      copy:
        src: "/path/to/your/application.jar"
        dest: "/opt/application.jar"

    - name: Start Java Application
      shell: nohup java -jar /opt/application.jar > /dev/null 2>&1 &
```

Provision Web and Database Servers
After setting up the Ansible control server, use the 6-provision-app-servers.yaml playbook to provision both the web and database servers. This playbook should create two EC2 instances—one for the web server with a public IP address and another for the database server in a private subnet without a public IP.

```bash
ansible-playbook 6-provision-app-servers.yaml --extra-vars "aws_region=eu-west-3 key_name=ansible-managed-server-key subnet_id_web=public-subnet-id subnet_id_db=private-subnet-id ami_id=ami-id-for-ubuntu"
```

Ensure to replace public-subnet-id and private-subnet-id with the actual subnet IDs from your AWS VPC setup. The ami-id-for-ubuntu should be replaced with the AMI ID of an Ubuntu image suitable for your AWS region.

## Configure Web and Database Servers
Once the servers are provisioned, you'll need to configure them. This involves setting up MySQL on the database server and deploying the Java web application on the web server.

6-configure-app-servers.yaml
Create a playbook named 6-configure-app-servers.yaml for this purpose. For MySQL installation, it's recommended to use an existing Ansible role, such as geerlingguy.mysql, which can be installed via ansible-galaxy:

```bash
ansible-galaxy install geerlingguy.mysql
```

The playbook might look like this:

```yaml 
---
- name: Configure Database Server
  hosts: db_server
  become: true
  roles:
    - geerlingguy.mysql
  vars:
    mysql_root_password: "secure_password"
    mysql_databases:
      - name: "myapp_db"

- name: Configure Web Server
  hosts: web_server
  become: true
  tasks:
    - name: Install Java
      apt:
        name: openjdk-11-jdk
        state: present
        update_cache: true

    - name: Deploy Java Web Application
      copy:
        src: "/path/to/your/application.jar"
        dest: "/var/myapp/application.jar"

    - name: Ensure Application is Running
      shell: nohup java -jar /var/myapp/application.jar > /var/log/myapp.log 2>&1 &
```

This playbook configures the MySQL server with a root password and a database for your app on the database server. On the web server, it installs Java and deploys your Java application.

Executing the Playbooks from Ansible Control Server
SSH into your Ansible control server:

```bash
ssh -i ~/Downloads/ansible-control-server-key.pem ubuntu@ansible-server-public-ip
```

Ensure you have copied your Ansible playbooks to this server or cloned the relevant Git repository containing them.

Run the playbook to provision the web and database servers:

```bash
ansible-playbook 6-provision-app-servers.yaml --extra-vars "..."
```

Followed by the playbook to configure the servers:

```bash
ansible-playbook -i 6-inventory_aws_ec2.yaml 6-configure-app-servers.yaml
```

Verifying the Deployment
After running the 6-configure-app-servers.yaml playbook, verify the Java application is running and accessible. Since the database server is in a private subnet, it will not be directly accessible, but the web server should communicate with it without issues, provided the VPC and security group configurations allow internal VPC traffic.

Ensure the security group for the web server allows inbound traffic on port 8080 to access the application via http://web-server-public-ip:8080.

This comprehensive approach leverages Ansible for infrastructure automation, allowing you to dynamically provision and configure servers on AWS for deploying traditional applications in a modern infrastructure setup.



# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/configuration-management-with-ansible/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
